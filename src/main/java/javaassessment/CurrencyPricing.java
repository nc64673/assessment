/**
 * @author Nicole Chow
 */

package javaassessment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CurrencyPricing {
	
	public ArrayList<String> list;
	public ArrayList<Float> rates;
	
	
	public CurrencyPricing(String file){
		list = new ArrayList<String>();
		rates = new ArrayList<Float>();
		updateRate(file);

	}
	
	@SuppressWarnings("resource")
	public void updateRate(String file){
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(file));
			String line = in.readLine();
			line = in.readLine();

			while (line!= null){
			
				List<String> rate = Arrays.asList(line.split(","));
				
				int i = list.indexOf(rate.get(0) + " " + rate.get(1));
				
				try{
					if (i >= 0 && Float.valueOf(rate.get(2)) != null){
						rates.set(i, Float.valueOf(rate.get(2)));
					} else {
						list.add(rate.get(0) + " " + rate.get(1));
						rates.add(Float.valueOf(rate.get(2)));
					}
				} catch(IndexOutOfBoundsException e){
					System.out.println("Missing price for " + rate.get(0) + "," + rate.get(1));
				} catch(NumberFormatException e){
					System.out.println("Invalid price for " + rate.get(0) + "," + rate.get(1));
				}
				line = in.readLine();
			}
		} catch (FileNotFoundException e1) {
			System.out.println("File DNE");
		} catch (IOException e){
			System.out.println("Error reading next line");
		}
	}
	
	public float getFXQuote(String c1, String c2) throws Exception{
		int i = list.indexOf(c1 + " " + c2);
		if (i >= 0){
			return rates.get(i);
		} else {
			int j = list.indexOf(c1 + " USD");
			int k = list.indexOf("USD " + c2);
			if (j >= 0 && k >= 0){
				return (rates.get(j) * rates.get(k));
			} else {
				throw new Exception();
			}
		}
	}
}
