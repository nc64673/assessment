/**
 * @author Nicole Chow
 */

package javaassessment;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.*;

import org.junit.Test;

public class CurrencyPricingTest {
	
	public CurrencyPricing prices = new  CurrencyPricing("rates.csv");
	
	@Test
	public void noCrossTest()throws Exception{
		try{
			assertEquals(4.2921, prices.getFXQuote("ZAR",  "TWD"), 0.0001f);
		} catch(Exception e){
			fail();
		}
	}
	
	@Test
	public void crossTest()throws Exception{
		try{
			assertEquals(0.1195, prices.getFXQuote("MXN",  "BGN"), 0.0001f);
		} catch(Exception e){
			fail();
		}
	}
	
	@Test(expected = Exception.class)
	public void noRate() throws Exception{
		prices.getFXQuote("USD", "FAQ");
	}
	
	@Test
	public void invalidFile() throws Exception{
		CurrencyPricing x = new CurrencyPricing("rate.csv");
		assertEquals(0, x.list.size());
	}
	
	@Test (expected = Exception.class)
	public void invalidPrice() throws Exception{
		prices.updateRate("rates1.csv");
		assertEquals(0.1505, prices.getFXQuote("ZAR,", "USD"), 0.0001f);
	}
	
	@Test
	public void changePrice() throws Exception{
		prices.updateRate("rates1.csv");
		assertEquals(0.1234, prices.getFXQuote("PEN",  "GBP"), 0.001f);
	}

}
